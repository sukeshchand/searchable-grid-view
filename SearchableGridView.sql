
/****** Object:  UserDefinedTableType [dbo].[SearchFilters]    Script Date: 01/02/2014 12:44:29 AM ******/
CREATE TYPE [dbo].[SearchFilters] AS TABLE(
	[SearchField] [varchar](255) NULL,
	[SearchValue] [varchar](255) NULL
)
GO
/****** Object:  StoredProcedure [dbo].[stp_ClientMaster_SearchForPagedData]    Script Date: 01/02/2014 12:44:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[stp_ClientMaster_SearchForPagedData]
(
	@SearchFilter		SearchFilters readonly,
	@SortExpression	varchar(250),
	@SortDirection		varchar(250),
	@PageIndex			int,
	@PageSize			int
)
AS
BEGIN
	 Declare @SQLString Varchar(Max);
	 Set @SQLString = '
	 SELECT * FROM
	 (
	 SELECT 
	 COUNT(*) OVER () TotalRows,
	 ROW_NUMBER() OVER (ORDER BY ' + @SortExpression + ' ' + @SortDirection + ') AS RowNumber,
	 dbo.ClientMaster.ClientID, dbo.ClientMaster.ClientCode, dbo.ClientMaster.ClientName, dbo.ClientMaster.DateOfBirth, dbo.ClientMaster.AddedUserID, 
     dbo.UserMaster.UserName AS AddedUserName, Convert(Varchar(10), dbo.ClientMaster.AddedDate, 103) + '' '' + CONVERT(VARCHAR, dbo.ClientMaster.AddedDate, 108) As AddedDate
	 FROM dbo.ClientMaster INNER JOIN
     dbo.UserMaster ON dbo.ClientMaster.AddedUserID = dbo.UserMaster.UserID
	'
	+
	dbo.fn_GetSearchFilterString(@SearchFilter)
	+
	'
	)TableList 
	WHERE RowNumber BETWEEN 
	((' + Convert(Varchar(20), @PageIndex) + ' - 1) * ' + Convert(Varchar(20), @PageSize) + ') + 1 
	AND ' + Convert(Varchar(20), @PageSize) + ' * ' + '(' + Convert(Varchar(20), @PageIndex) + ');
	'
	Exec(@SQLString);
End

GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetSearchFilterString]    Script Date: 01/02/2014 12:44:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create  FUNCTION [dbo].[fn_GetSearchFilterString] 
(
	 @SearchFilter SearchFilters readonly
)
RETURNS varchar(Max)
AS
BEGIN
	--Variable for fetching data from data table
	Declare @SearchField Varchar(255);
	Declare @SearchValue Varchar(255);

	--variable to build Search Filter String
	Declare @SearchFilterString Varchar(Max) = '';

	--Declareing and open cursor for read from table variable line by line
	Declare Cur CURSOR LOCAL For
    Select SearchField, SearchValue From @SearchFilter;
	
	Open Cur;
	Fetch Next From Cur Into @SearchField, @SearchValue;
	While @@FETCH_STATUS = 0 
	BEGIN
		Set @SearchValue = Replace(@SearchValue, '*','%');
		IF @SearchValue = ''
			Begin
				Set @SearchValue = '%';
			End
		Set @SearchFilterString = @SearchFilterString + ' And (IsNull(Convert(Varchar(100), ' + @SearchField + '),'''') Like ''' + @SearchValue + ''')';
		Fetch Next From Cur Into @SearchField, @SearchValue;
	End
IF (@SearchFilterString <> '')
	Begin
		Set @SearchFilterString = ' Where  ( ' + Right(@SearchFilterString, LEN(@SearchFilterString) - 5) + ' )';
	End
Close Cur;
Deallocate Cur;
Return @SearchFilterString;

END
GO
/****** Object:  Table [dbo].[ClientMaster]    Script Date: 01/02/2014 12:44:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ClientMaster](
	[ClientID] [int] NOT NULL,
	[ClientCode] [varchar](10) NOT NULL,
	[ClientName] [varchar](50) NOT NULL,
	[DateOfBirth] [date] NOT NULL,
	[AddedUserID] [smallint] NOT NULL,
	[AddedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_ClientMaster] PRIMARY KEY CLUSTERED 
(
	[ClientID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserMaster]    Script Date: 01/02/2014 12:44:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserMaster](
	[UserID] [smallint] NOT NULL,
	[UserName] [varchar](20) NOT NULL,
 CONSTRAINT [PK_UserMaster] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[ClientMaster] ([ClientID], [ClientCode], [ClientName], [DateOfBirth], [AddedUserID], [AddedDate]) VALUES (1, N'0001', N'Rajeesh Lal', CAST(0x9D070B00 AS Date), 1, CAST(0x0000A157017F4939 AS DateTime))
INSERT [dbo].[ClientMaster] ([ClientID], [ClientCode], [ClientName], [DateOfBirth], [AddedUserID], [AddedDate]) VALUES (2, N'0002', N'Bobson P Samuel', CAST(0xD80A0B00 AS Date), 1, CAST(0x00009FE9017F517C AS DateTime))
INSERT [dbo].[ClientMaster] ([ClientID], [ClientCode], [ClientName], [DateOfBirth], [AddedUserID], [AddedDate]) VALUES (3, N'0003', N'Sowrabh Malhotra', CAST(0x810A0B00 AS Date), 1, CAST(0x00009FE9017F518A AS DateTime))
INSERT [dbo].[ClientMaster] ([ClientID], [ClientCode], [ClientName], [DateOfBirth], [AddedUserID], [AddedDate]) VALUES (4, N'0004', N'Saji Mon', CAST(0x9D070B00 AS Date), 1, CAST(0x0000A2C4017F5197 AS DateTime))
INSERT [dbo].[ClientMaster] ([ClientID], [ClientCode], [ClientName], [DateOfBirth], [AddedUserID], [AddedDate]) VALUES (5, N'0005', N'Lal G George', CAST(0xD80A0B00 AS Date), 1, CAST(0x0000A2C4017F51A3 AS DateTime))
INSERT [dbo].[ClientMaster] ([ClientID], [ClientCode], [ClientName], [DateOfBirth], [AddedUserID], [AddedDate]) VALUES (6, N'0006', N'Sajith Kumar', CAST(0x810A0B00 AS Date), 1, CAST(0x0000A2C4017F51B0 AS DateTime))
INSERT [dbo].[ClientMaster] ([ClientID], [ClientCode], [ClientName], [DateOfBirth], [AddedUserID], [AddedDate]) VALUES (7, N'0007', N'Vipin Job', CAST(0x9D070B00 AS Date), 1, CAST(0x0000A2C4017F51BD AS DateTime))
INSERT [dbo].[ClientMaster] ([ClientID], [ClientCode], [ClientName], [DateOfBirth], [AddedUserID], [AddedDate]) VALUES (8, N'0008', N'Monoj Kumar', CAST(0xD80A0B00 AS Date), 1, CAST(0x0000A2C4017F51CA AS DateTime))
INSERT [dbo].[ClientMaster] ([ClientID], [ClientCode], [ClientName], [DateOfBirth], [AddedUserID], [AddedDate]) VALUES (9, N'0009', N'Raj Kumar', CAST(0x810A0B00 AS Date), 2, CAST(0x0000A2C4017F51D6 AS DateTime))
INSERT [dbo].[ClientMaster] ([ClientID], [ClientCode], [ClientName], [DateOfBirth], [AddedUserID], [AddedDate]) VALUES (10, N'0010', N'Mohan Kumar', CAST(0x810A0B00 AS Date), 2, CAST(0x0000A2C4017F51E3 AS DateTime))
INSERT [dbo].[ClientMaster] ([ClientID], [ClientCode], [ClientName], [DateOfBirth], [AddedUserID], [AddedDate]) VALUES (11, N'0011', N'Syam Kumar', CAST(0x9D070B00 AS Date), 2, CAST(0x0000A2C4017F51F0 AS DateTime))
INSERT [dbo].[ClientMaster] ([ClientID], [ClientCode], [ClientName], [DateOfBirth], [AddedUserID], [AddedDate]) VALUES (12, N'0012', N'Manu', CAST(0xD80A0B00 AS Date), 2, CAST(0x0000A2C4017F51FD AS DateTime))
INSERT [dbo].[ClientMaster] ([ClientID], [ClientCode], [ClientName], [DateOfBirth], [AddedUserID], [AddedDate]) VALUES (13, N'0013', N'Manju', CAST(0x810A0B00 AS Date), 2, CAST(0x0000A2C4017F5209 AS DateTime))
INSERT [dbo].[ClientMaster] ([ClientID], [ClientCode], [ClientName], [DateOfBirth], [AddedUserID], [AddedDate]) VALUES (14, N'0014', N'Mahesh Varghese', CAST(0x810A0B00 AS Date), 2, CAST(0x0000A2C4017F5216 AS DateTime))
INSERT [dbo].[ClientMaster] ([ClientID], [ClientCode], [ClientName], [DateOfBirth], [AddedUserID], [AddedDate]) VALUES (15, N'0015', N'Josekutty', CAST(0x9D070B00 AS Date), 2, CAST(0x0000A2C4017F5223 AS DateTime))
INSERT [dbo].[ClientMaster] ([ClientID], [ClientCode], [ClientName], [DateOfBirth], [AddedUserID], [AddedDate]) VALUES (16, N'0016', N'Ramakrishna', CAST(0xD80A0B00 AS Date), 2, CAST(0x0000A2C4017F522F AS DateTime))
INSERT [dbo].[ClientMaster] ([ClientID], [ClientCode], [ClientName], [DateOfBirth], [AddedUserID], [AddedDate]) VALUES (17, N'0017', N'Abhinav', CAST(0x810A0B00 AS Date), 1, CAST(0x0000A2C4017F523C AS DateTime))
INSERT [dbo].[ClientMaster] ([ClientID], [ClientCode], [ClientName], [DateOfBirth], [AddedUserID], [AddedDate]) VALUES (18, N'0018', N'Kumar S', CAST(0x810A0B00 AS Date), 1, CAST(0x0000A2C4017F5248 AS DateTime))
INSERT [dbo].[ClientMaster] ([ClientID], [ClientCode], [ClientName], [DateOfBirth], [AddedUserID], [AddedDate]) VALUES (19, N'0019', N'Sam Ponnuse', CAST(0x9D070B00 AS Date), 1, CAST(0x0000A2C4017F5255 AS DateTime))
INSERT [dbo].[ClientMaster] ([ClientID], [ClientCode], [ClientName], [DateOfBirth], [AddedUserID], [AddedDate]) VALUES (20, N'0020', N'Namitha', CAST(0xD80A0B00 AS Date), 1, CAST(0x0000A2C4017F5261 AS DateTime))
INSERT [dbo].[ClientMaster] ([ClientID], [ClientCode], [ClientName], [DateOfBirth], [AddedUserID], [AddedDate]) VALUES (21, N'0021', N'Vivek P', CAST(0x810A0B00 AS Date), 1, CAST(0x0000A2C4017F526E AS DateTime))
INSERT [dbo].[ClientMaster] ([ClientID], [ClientCode], [ClientName], [DateOfBirth], [AddedUserID], [AddedDate]) VALUES (22, N'0022', N'Pranav', CAST(0x810A0B00 AS Date), 1, CAST(0x0000A2C4017F527B AS DateTime))
INSERT [dbo].[ClientMaster] ([ClientID], [ClientCode], [ClientName], [DateOfBirth], [AddedUserID], [AddedDate]) VALUES (23, N'0023', N'Deepu Mohan', CAST(0x9D070B00 AS Date), 1, CAST(0x0000A2C4017F5288 AS DateTime))
INSERT [dbo].[ClientMaster] ([ClientID], [ClientCode], [ClientName], [DateOfBirth], [AddedUserID], [AddedDate]) VALUES (24, N'0024', N'Ajeesh Ayappan', CAST(0xD80A0B00 AS Date), 1, CAST(0x0000A2C4017F5294 AS DateTime))
INSERT [dbo].[ClientMaster] ([ClientID], [ClientCode], [ClientName], [DateOfBirth], [AddedUserID], [AddedDate]) VALUES (25, N'0025', N'Anjana Krishnan', CAST(0x810A0B00 AS Date), 1, CAST(0x0000A2C4017F52A2 AS DateTime))
INSERT [dbo].[ClientMaster] ([ClientID], [ClientCode], [ClientName], [DateOfBirth], [AddedUserID], [AddedDate]) VALUES (26, N'0026', N'Gana Swatthy', CAST(0x9D070B00 AS Date), 1, CAST(0x0000A2C4017F52AE AS DateTime))
INSERT [dbo].[UserMaster] ([UserID], [UserName]) VALUES (1, N'sukeshchand')
INSERT [dbo].[UserMaster] ([UserID], [UserName]) VALUES (2, N'admin')
ALTER TABLE [dbo].[ClientMaster] ADD  CONSTRAINT [DF_ClientMaster_AddedDate]  DEFAULT (getdate()) FOR [AddedDate]
GO
USE [master]
GO
ALTER DATABASE [TEST] SET  READ_WRITE 
GO
