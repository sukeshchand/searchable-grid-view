﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default_2.aspx.cs" Inherits="SoftToRule.Client.Default_2" %>

<%@ Register Assembly="SoftToRule.SearchableGridView" Namespace="SoftToRule.WebControls"
    TagPrefix="SoftToRule" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <b><u>Example - 2</u></b>
                            </td>
                            <td>
                            |
                            </td>
                            <td>
                                <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/Default.aspx"> Go to Example - 1</asp:HyperLink>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <SoftToRule:SoftToRuleGridView ID="sgvSearchClientMaster" runat="server" AllowSorting="true"
                        AutoGenerateColumns="False" CancelSearchImageURL="~/Icons/filterCancel.png" CellPadding="4"
                        ForeColor="#333333" OnFilterButtonClick="sgvSearchClientMaster_FilterButtonClick"
                        OnSelectedIndexChanged="sgvSearchClientMaster_SelectedIndexChanged" SearchGoFirstImageUrl="~/Icons/SearchGoFirst.png"
                        SearchGoImageUrl="~/Icons/SearchGo.png" SearchGoLastImageUrl="~/Icons/SearchGoLast.png"
                        SearchGoNextImageUrl="~/Icons/SearchGoNext.png" SearchGoPreviousImageUrl="~/Icons/SearchGoPrevious.png"
                        SearchImageURL="~/Icons/filter.png" ShowFooter="True" CurrentSortDirection="ASC"
                        CurrentSortExpression="ClientMaster.ClientID" Font-Size="Small" Font-Names="Segoe UI"
                        ShowRowNumber="False" OnCancelFilterButtonClick="sgvSearchClientMaster_CancelFilterButtonClick"
                        OnNavigationButtonClick="sgvSearchClientMaster_NavigationButtonClick" OnSorting="sgvSearchClientMaster_Sorting">
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>
                            <SoftToRule:SearchBoundField DataField="ClientID" HeaderText="" SearchExpression="ClientMaster.ClientID"
                                Visible="false">
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle Width="0px" HorizontalAlign="Left" />
                            </SoftToRule:SearchBoundField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Default.aspx?ID=1">click me</asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <SoftToRule:SearchBoundField DataField="ClientCode" HeaderText="Client Code" SearchExpression="ClientMaster.ClientCode"
                                SortExpression="ClientMaster.ClientCode">
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle Width="80px" HorizontalAlign="Left" />
                            </SoftToRule:SearchBoundField>
                            <SoftToRule:SearchBoundField DataField="ClientName" HeaderText="Client Name" SearchExpression="ClientMaster.ClientName"
                                SortExpression="ClientMaster.ClientName">
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle Width="180px" HorizontalAlign="Left" />
                            </SoftToRule:SearchBoundField>
                            <SoftToRule:SearchBoundField DataField="DateOfBirth" HeaderText="Date of Birth" SearchExpression="Convert(varchar(10), ClientMaster.DateOfBirth, 103)"
                                DataFormatString="{0:dd/MM/yyyy}" SortExpression="ClientMaster.DateOfBirth">
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle Width="100px" HorizontalAlign="Left" />
                            </SoftToRule:SearchBoundField>
                            <SoftToRule:SearchBoundField DataField="AddedUserName" HeaderText="Added By" SearchExpression="UserMaster.UserName"
                                SortExpression="UserMaster.UserName">
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle Width="100px" HorizontalAlign="Left" />
                            </SoftToRule:SearchBoundField>
                            <SoftToRule:SearchBoundField DataField="AddedDate" HeaderText="Added Date" SearchExpression="Convert(Varchar(10), dbo.ClientMaster.AddedDate, 103) + ' ' + CONVERT(VARCHAR, dbo.ClientMaster.AddedDate, 108)"
                                SortExpression="ClientMaster.AddedDate" DataFormatString="{0:dd/MM/yyyy HH:mm:ss}">
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle Width="200px" HorizontalAlign="Left" />
                            </SoftToRule:SearchBoundField>
                        </Columns>
                        <EditRowStyle BackColor="#2461BF" />
                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                        <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    </SoftToRule:SoftToRuleGridView>
                </td>
            </tr>
            <tr>
                <td>
                    <hr />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
