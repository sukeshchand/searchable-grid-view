﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using SoftToRule.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace SoftToRule.Client
{
    public partial class Default_2 : System.Web.UI.Page
    {
        string strMsg;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                sgvSearchClientMaster.ClearSearchFilters();
                FilterToNthPage(sgvSearchClientMaster.SearchFilters, sgvSearchClientMaster.CurrentSortExpression, (string)sgvSearchClientMaster.CurrentSortDirection, 1);
            }
        }

        public void sgvSearchClientMaster_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                strMsg = "Client ID: " + sgvSearchClientMaster.Rows[sgvSearchClientMaster.SelectedIndex].Cells[0].Text.Trim() + " Selected";

            }
            catch (System.Exception ex)
            {
                strMsg = ex.Message;
            }
            finally
            {
                lblMsg.Text = strMsg;
            }
        }

        public void sgvSearchClientMaster_FilterButtonClick(object sender, SearchGridEventArgs e)
        {
            FilterToNthPage(e.SearchFilterValues, sgvSearchClientMaster.CurrentSortExpression, sgvSearchClientMaster.CurrentSortDirection, 1);
        }

        public void sgvSearchClientMaster_CancelFilterButtonClick(object sender, SearchGridEventArgs e)
        {
            FilterToNthPage(e.SearchFilterValues, sgvSearchClientMaster.CurrentSortExpression, sgvSearchClientMaster.CurrentSortDirection, 1);
        }

        public void sgvSearchClientMaster_NavigationButtonClick(object sender, NavigationButtonEventArgs e)
        {
            if (e.NavigationButtonsType == NavigationButtonsTypes.GoFirst)
            {
                FilterToNthPage(sgvSearchClientMaster.SearchFilters,
                    sgvSearchClientMaster.CurrentSortExpression,
                    sgvSearchClientMaster.CurrentSortDirection, 1);
            }
            else if (e.NavigationButtonsType == NavigationButtonsTypes.GoLast)
            {
                FilterToNthPage(sgvSearchClientMaster.SearchFilters,
                    sgvSearchClientMaster.CurrentSortExpression,
                    sgvSearchClientMaster.CurrentSortDirection,
                    sgvSearchClientMaster.TotalSearchPages);
            }
            else if (e.NavigationButtonsType == NavigationButtonsTypes.GoNext)
            {
                if (sgvSearchClientMaster.CurrentSearchPageNo < sgvSearchClientMaster.TotalSearchPages)
                {
                    FilterToNthPage(sgvSearchClientMaster.SearchFilters,
                        sgvSearchClientMaster.CurrentSortExpression,
                        sgvSearchClientMaster.CurrentSortDirection,
                        (int)sgvSearchClientMaster.CurrentSearchPageNo + 1);
                }
            }
            else if (e.NavigationButtonsType == NavigationButtonsTypes.GoPrevious)
            {
                if (sgvSearchClientMaster.CurrentSearchPageNo > 1)
                {
                    FilterToNthPage(sgvSearchClientMaster.SearchFilters,
                        sgvSearchClientMaster.CurrentSortExpression,
                        sgvSearchClientMaster.CurrentSortDirection,
                        (int)sgvSearchClientMaster.CurrentSearchPageNo - 1);
                }
            }
            else if (e.NavigationButtonsType == NavigationButtonsTypes.GoToPage)
            {
                FilterToNthPage(sgvSearchClientMaster.SearchFilters,
                    sgvSearchClientMaster.CurrentSortExpression,
                    sgvSearchClientMaster.CurrentSortDirection,
                    (int)e.PageIndex);
            }
        }

        public void sgvSearchClientMaster_Sorting(object sender, GridViewSortEventArgs e)
        {
            FilterToNthPage(sgvSearchClientMaster.SearchFilters, sgvSearchClientMaster.CurrentSortExpression, sgvSearchClientMaster.CurrentSortDirection, (int)sgvSearchClientMaster.CurrentSearchPageNo);
        }

        private void FilterToNthPage(DataTable SearchFilterValues, string SortExpression, string SortDirection, int PageIndex)
        {
            int PageSize = sgvSearchClientMaster.PageSize;

            try
            {
                DataTable dt = GetSearchFilteredData("stp_ClientMaster_SearchForPagedData", "@SearchFilter", SearchFilterValues, SortExpression, SortDirection, PageIndex, PageSize);
                sgvSearchClientMaster.CurrentSearchPageNo = PageIndex;
                if (dt.Rows.Count > 0)
                {
                    sgvSearchClientMaster.TotalSearchRecords = (int)dt.Rows[0]["TotalRows"];
                }
                else
                {
                    sgvSearchClientMaster.TotalSearchRecords = 0;
                }
                sgvSearchClientMaster.DataSource = dt;
                sgvSearchClientMaster.DataBind();
            }
            catch (System.Exception ex)
            {
                strMsg = ex.Message;
            }
            finally
            {
                lblMsg.Text = strMsg;
            }
        }

        public DataTable GetSearchFilteredData(string StoreProcedureName, string SQLTableVariableName, DataTable SearchFilterValues, string SortExpression, string SortDirection, int PageIndex, int PageSize)
        {
            // Assumes connection is an open SqlConnection object.
            SqlConnection conn = new SqlConnection("Password=welcome;Persist Security Info=True;User ID=sa;Initial Catalog=TEST;Data Source=Unnikuttan");
            using (conn)
            {
                //Open the connection
                conn.Open();

                // Configure the SqlCommand and SqlParameter.
                SqlCommand cmd = new SqlCommand(StoreProcedureName, conn);
                cmd.CommandType = CommandType.StoredProcedure;

                //creating parameter and assign values
                //Create SQL Table parameter and assign datatable to it
                SqlParameter tvpParam = cmd.Parameters.AddWithValue(SQLTableVariableName, SearchFilterValues);
                tvpParam.SqlDbType = SqlDbType.Structured;

                SqlParameter stpPageIndex = cmd.Parameters.AddWithValue("@PageIndex", PageIndex);
                stpPageIndex.SqlDbType = SqlDbType.Int;

                SqlParameter stpPageSize = cmd.Parameters.AddWithValue("@PageSize", PageSize);
                stpPageSize.SqlDbType = SqlDbType.Int;

                SqlParameter stpSortExpression = cmd.Parameters.AddWithValue("@SortExpression", SortExpression);
                stpSortExpression.SqlDbType = SqlDbType.VarChar;

                SqlParameter stpSortDirection = cmd.Parameters.AddWithValue("@SortDirection", SortDirection);
                stpSortDirection.SqlDbType = SqlDbType.VarChar;

                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);

                //Close connection
                conn.Close();
                return ds.Tables[0];
            }
        }
    }
}